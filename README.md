[[__TOC__]

# KOPERASI  XYZ (belum ada namanya, masih sedang kumpulkan nama dari teman2 semua)
1. Rumah Bahagia Bersama / RBB  (cari nama 3 suku kata)
2. Aku Jadi Berkat
3. …..   silahkan jika ada usulan lain dari teman2 ..

## Visi Misi
VISI : Menjangkau orang-orang untuk dapat merasakan anugerah dan kasih Sang Pencipta
Untuk agama Katolik dan Kristiani :   2 Korintus 8:14 “Maka hendaklah sekarang ini kelebihan kamu mencukupkan kekurangan mereka, agar kelebihan mereka kemudian mencukupkan kekurangan kamu, supaya ada keseimbangan”

## MISI :
1.	giving and sharing (kasih)    dan   happiness (kebahagiaan) , kalangan internal dahulu sesudah itu kepada kalangan luar dan orang banyak
2.	memberikan pembekalan kepada remaja pemuda, kalangan internal dahulu sesudah itu kepada kalangan luar dan orang banyak
3.	keanggotaan koperasi bisa turun temurun, kita mau mewariskan nilai2 positif, kebaikan dan kebahagiaan ke generasi kita selanjutnya …

TUJUAN utama nya adalah SOSIAL/pelayanan (membagikan kasih & kebahagiaan sebesar2nya) dan bukan bisnis (mendapatkan kekayaan sebesar2nya).
Mindset bergabungnya adalah karena kita ingin sama2 berbagi …


# Usulan susunan pengurus Koperasi
Ketua Koperasi : Lufi Rachmad
Wakil Ketua Koperasi : Aldo Fantinus
Sekretaris : Oscar
Pengurus lainnya : akan disusun kemudian

 
# Usulan Bisnis2 yang bisa dijalankan :
1. TRADING :
    • membuat toko online untuk menjual barang2 member kepada member pada awalnya, selanjutnya akan dipromosikan kepada eksternal (termasuk IA TOP). Sarana promo melalui social media yang ekonomis secara biaya.

    • Produk2 dari vendor member akan ditentukan oleh pengurus apakah akan menggunakan brand OEM koperasi ataukah tetap menggunakan brand original nya.

    • Margin (keuntungan) dari produk vendor akan dinegosiasi dan disepakati bersama.

    • Contoh saat ini produk2 tsb antara lain : member Fero (beras merah, snack, dll),  member Tjahjadi (healthy broth, all purpose sauce).

2. PERTANIAN dan FOOD PROCESSING :
    * Sedang development untuk memiliki usaha pertanian yang produktif dengan bekerja sama dengan petani plasma.
    * Sedang development food processing untuk meningkatkan nilai dari produk pertanian yang dihasilkan.

3. CONSULTING dan COACHING :
* Sedang development untuk mencari kebutuhan client dalam hal pelatihan yang diperlukan saat ini.
* Koperasi memiliki member-member yang memiliki profil dan kompeten di bidang nya masing2 untuk memberikan pelatihan tsb, antara lain : Aldo, Buhe, San2, Lufi, Catharine, Nancy istri Siangi, dan lainnya.

# IURAN dan PERMODALAN

## IURAN POKOK :
### Iuran pokok adalah sebesar Rp. 3.000.000, disetorkan satu kali saja pada saat anggota baru bergabung koperasi.
### Iuran pokok dapat dikembalikan kepada anggota yang mengundurkan diri di kemudian hari dengan syarat waktu pengembaliannya baru dapat dilakukan pada waktu masa keanggotaan nya berumur minimal 3 tahun.

## IURAN WAJIB :
### Iuran wajib adalah sebesar Rp. 600.000, disetorkan setiap 6 bulan dimuka secara rutin.
### Iuran wajib dapat dikembalikan kepada anggota yang mengundurkan diri di kemudian hari dengan syarat waktu pengembaliannya baru dapat dilakukan pada waktu masa keanggotaan nya berumur minimal 3 tahun.

Iuran Pokok dan Iuran Wajib akan dipergunakan untuk membiayai biaya legalitas koperasi misalnya biaya pembuatan akte koperasi dll   dan membiayai operasional dasar koperasi, misalnya biaya gaji staf pemasaran, biaya sewa kantor coworking space (jika ada) dan biaya lain yang akan diputuskan di dalam rapat anggota.  Koperasi senantiasa meninjau dan melakukan efisiensi biaya2 rutin tersebut jika dipandang tidak efisien dan keuntungan usaha koperasi tidak dapat menutup biaya operasional yang berjalan.

## IURAN

### IURAN SUKARELA – PINJAMAN
* Modal pinjaman dari anggota, akan diperhitungkan bunga pinjaman yang disepakati bersama dengan koperasi setelah memperhitungan keuntungan dari proyek yang didanai. Sisa keuntungan proyek akan menjadi hak koperasi.
* Pinjaman akan dikembalikan setelah project yang didanai telah selesai.

### IURAN SUKARELA – HIBAH
* Modal hibah dari anggota, seluruhnya akan menjadi hak koperasi tanpa ada kewajiban dari koperasi untuk mengembalikan dana hibah tersebut.

Iuran sukarela akan dipergunakan untuk membiayai suatu project inisiatif dari satu atau beberapa anggota koperasi yang berkomitment untuk menjalankan project tsb.  Misalnya dalam project food processing, Siangi dan Koyo akan menyetorkan iuran sukarela untuk mendanai project tsb dengan tujuan untuk memperoleh bunga pinjaman bagi pemodal dan sisa keuntungan bagi koperasi.

## SISA HASIL USAHA (SHU)
Pembukuan dan pengelolaan koperasi akan dilakukan secara professional. SHU akan dihitung secara tahuan pada setiap tanggal 30 Juni  dan akan dibagikan kepada anggota pada setiap tanggal 31 Juli. Target SHU untuk setiap anggota adalah minimal sebesar tingkat bunga deposito yang berlaku saat itu.
Penghitungan SHU tersebut akan diusulkan di rapat anggota setelah mempertimbangkan juga mengenai kondisi keuangan koperasi, dana cadangan usaha, dll.

## NEXT STEP
### Mengumpulkan minimal 20 orang anggota sebagai syarat minimum jumlah Dinas Koperasi Bandung melakukan sosialisasi mengenai seluk beluk koperasi dan akan dibuat berita acara resmi nya.
### Melakukan rapat anggota pendiri untuk membuat AD ART koperasi dimana seluruh hal-hal teknis mengenai koperasi diputuskan bersama-sama (misalnya kapan diadakan rapat anggota, dan hal-hal lainnya).
### Setelah sosialisasi DinKop, sambil menunggu akte pendirian koperasi formal disahkan, kita bisa mulai mengumpulkan iuran pokok dan wajib di rekening Pribadi bendahara yang kita percayakan serta mulai menjalankan bisnis dan operasional selayaknya seperti koperasi.

